#pragma once

/* IdGenerator.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <cstdint>
#include <vector>

namespace dynamicreflectance {
namespace common
{

    class IdGenerator
    {
    public:

        IdGenerator(std::int32_t startId)
            : firstFreeId(startId)
        {}

        ~IdGenerator()
        {}

        std::int32_t getNextId() const
        {
            return this->firstFreeId++;
        }

        bool isTaken(std::int32_t id) const
        {
            return this->firstFreeId >= id;
        }

    private:

        mutable std::int32_t firstFreeId;
    };

}
}