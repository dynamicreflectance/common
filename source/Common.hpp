#pragma once

/* Common.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Assert.hpp"

#include "Vector2.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"
#include "Plane.hpp"
#include "Matrix4x4.hpp"
#include "Frustum.hpp"
#include "AABB2.hpp"
#include "AABB3.hpp"
#include "Math.hpp"

#include "InOut.hpp"
#include "Out.hpp"

#include "Color4.hpp"
#include "ColorFormat.hpp"
#include "EntryLocation.hpp"
#include "ImageData.hpp"

#include "Logger.hpp"

#include "IdGenerator.hpp"

#include "Version.hpp"