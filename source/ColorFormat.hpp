#pragma once

/* ColorFormat.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Assert.hpp"

#include <cstdint>

namespace dynamicreflectance {
namespace common
{

	enum class ColorFormat
	{
		_UNKOWN = -1,
		RGB,
		RGBA,
        RED,
        DEPTH,
		_COUNT
	};

    inline std::int32_t getChannelsCount(ColorFormat format)
    {
        std::int32_t ret = 0;

        switch (format)
        {
            case ColorFormat::RGB:
            {
                ret = 3;
            }
            break;

            case ColorFormat::RGBA:
            {
                ret = 4;
            }
            break;

            case ColorFormat::DEPTH:
            case ColorFormat::RED:
            {
                ret = 1;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented ColorFormat case");
        }

        return ret;
    }

}
}