/* Assert.cpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*/

#include "Assert.hpp"
#include "Logger.hpp"

#include <cstdlib>

namespace
{
    dynamicreflectance::common::Logger *g_pLogger = nullptr;
}


namespace dynamicreflectance {
namespace common
{

    bool assertFunction(const char *condition, const char *message, const EntryLocation &location)
    {
        printf("Condition: \"%s\"\nMessage: \"%s\"\nFile: %s\nFunction: %s\nLOC: %d\n", condition, message, location.fileName.c_str(), location.functionName.c_str(), location.line);

        if (nullptr != g_pLogger)
        {
            g_pLogger->assert(location, "%s\n", message);
        }

        std::exit(-1);

        return true;
    }

    void setAssertLogger(Logger *pLogger)
    {
        g_pLogger = pLogger;
    }


}
}