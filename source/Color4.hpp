#pragma once

/* Color4.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Math.hpp"

namespace dynamicreflectance {
namespace common
{

    struct Color4
    {
        union
        {
            struct 
            {
                float r, g, b, a;
            };

            float channels[4];
        };

        Color4()
            : r(0.0f)
            , g(0.0f)
            , b(0.0f)
            , a(0.0f)
        {}

        Color4(float r, float g, float b, float a)
            : r(r)
            , g(g)
            , b(b)
            , a(a)
        {}

        bool operator == (const Color4 &rhs) const
        {
            return
                true == math::areScalarsEqual(this->r, rhs.r) &&
                true == math::areScalarsEqual(this->g, rhs.g) &&
                true == math::areScalarsEqual(this->b, rhs.b) &&
                true == math::areScalarsEqual(this->a, rhs.a)
           ;
        }
    };
}
}