#pragma once

/* Plane.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Vector3.hpp"
#include "Assert.hpp"
#include "Math.hpp"

namespace dynamicreflectance {
namespace common {
namespace math
{

    struct Plane
    {
        union
        {
            struct
            {
                float A;
                float B;
                float C;
            };

            Vector3 normal;
        };
        float D;


        Plane()
            : A(0.0f)
            , B(0.0f)
            , C(0.0f)
            , D(0.0f)
        {}

        Plane(float A, float B, float C, float D)
            : A(A)
            , B(B)
            , C(C)
            , D(D)
        {
            float invertedLength = 1.0f / sqrt(this->A * this->A + this->B * this->B + this->C * this->C);
            this->A *= invertedLength;
            this->B *= invertedLength;
            this->C *= invertedLength;
            this->D *= invertedLength;
        }

        Plane(Vector3 normal, Vector3 point)
            : normal(normal)
            , D(-A * point.x - B * point.y - C * point.z)
        {}

        Plane(Vector3 p0, Vector3 p1, Vector3 p2)
        {
            this->calculate(p0, p1, p2);
        }

        void set(float A, float B, float C, float D)
        {
            this->A = A;
            this->B = B;
            this->C = C;
            this->D = D;

            float invertedLength = 1.0f / sqrt(this->A * this->A + this->B * this->B + this->C * this->C);
            this->A *= invertedLength;
            this->B *= invertedLength;
            this->C *= invertedLength;
            this->D *= invertedLength;
        }

        void calculate(Vector3 p0, Vector3 p1, Vector3 p2)
        {
            Vector3 p0p1 = p1 - p0;
            Vector3 p0p2 = p2 - p0;

            Vector3 normal = cross(p0p1, p0p2);

            this->A = normal.x;
            this->B = normal.y;
            this->C = normal.z;
            this->D = -A * p0.x - B * p0.y - C * p0.z;
        }
     };

    inline float signedDistance(const Plane &plane, const Vector3 &point)
    {
        return plane.A * point.x + plane.B * point.y + plane.C * point.z + plane.D;
    }

    inline Plane normalize(const Plane &plane)
    {
        Plane ret;
        float invertedLength = 1.0f / sqrt(plane.A * plane.A + plane.B * plane.B + plane.C * plane.C);
        ret.A *= invertedLength;
        ret.B *= invertedLength;
        ret.C *= invertedLength;
        ret.D *= invertedLength;

        return ret;
    }


}
}
}
