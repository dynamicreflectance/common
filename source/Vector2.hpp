#pragma once

/* Vctor2.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Math.hpp"

namespace dynamicreflectance {
namespace common {
namespace math
{

    struct Vector2
    {
        union
        {
            struct
            {
                float x, y;
            };

            float linear[2];
        };

        Vector2()
            : x(0.0f)
            , y(0.0f)
        {}

        Vector2(float x, float y)
            : x(x)
            , y(y)
        {}

        void set(float x, float y)
        {
            this->x = x;
            this->y = y;
        }

        void setZero()
        {
            this->set(0.0f, 0.0f);
        }

        const Vector2& operator += (Vector2 rhs)
        {
            this->x += rhs.x;
            this->y += rhs.y;

            return *this;
        }

        const Vector2& operator -= (Vector2 rhs)
        {
            this->x -= rhs.x;
            this->y -= rhs.y;

            return *this;
        }
    };

    inline float squaredLength(const Vector2 &v)
    {
        return v.x * v.x + v.y * v.y;
    }

    inline float length(const Vector2 &v)
    {
        return sqrt(v.x * v.x + v.y * v.y);
    }

    inline Vector2 normalize(const Vector2 &v)
    {
        float invLen = 1.0f / length(v);
        return Vector2(v.x * invLen, v.y * invLen);
    }

    inline float dot(const Vector2 &a, const Vector2 &b)
    {
        return a.x * b.x + a.y * b.y;
    }

    inline Vector2 abs(const Vector2 &v)
    {
        return Vector2(std::abs(v.x), std::abs(v.y));
    }

    inline Vector2 operator * (const Vector2 &v, float f)
    {
        return Vector2(v.x * f, v.y * f);
    }

    inline Vector2 operator * (float factor, Vector2 rhs)
    {
        return Vector2(rhs.x * factor, rhs.y * factor);
    }

    inline Vector2 operator + (const Vector2 &a, const Vector2 &b)
    {
        return Vector2(a.x + b.x, a.y + b.y);
    }

    inline Vector2 operator - (const Vector2 &a, const Vector2 &b)
    {
        return Vector2(a.x - b.x, a.y - b.y);
    }

    inline bool operator == (const Vector2 &a, const Vector2 &b)
    {
        return  areScalarsEqual(a.x, b.x) &&
                areScalarsEqual(a.y, b.y)
       ;
    }

    inline bool operator != (const Vector2 &a, const Vector2 &b)
    {
        return false == (a == b);
    }

}
}
}