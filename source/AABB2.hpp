#pragma once

/* AABB2.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Vector3.hpp"

#include <algorithm>
#include <array>
#include <cmath>

namespace dynamicreflectance {
namespace common {
namespace math
{

    struct AABB2
    {
        common::math::Vector3 center;
        common::math::Vector2 halfSize;

        AABB2()
        {}

        AABB2(common::math::Vector3 center, common::math::Vector2 halfSize, float fatFactor = 0.0f)
            : center(center)
            , halfSize(halfSize)
        {
            this->halfSize.x += halfSize.x * fatFactor;
            this->halfSize.y += halfSize.y * fatFactor;
        }

        AABB2(const AABB2 &a, const AABB2 &b, float zPos)
        {
            common::math::Vector2 leftUpper1 = common::math::Vector2(a.center.x - a.halfSize.x, a.center.y + a.halfSize.y);
            common::math::Vector2 rightLower1 = common::math::Vector2(a.center.x + a.halfSize.x, a.center.y - a.halfSize.y);

            common::math::Vector2 leftUpper2 = common::math::Vector2(b.center.x - b.halfSize.x, b.center.y + b.halfSize.y);
            common::math::Vector2 rightLower2 = common::math::Vector2(b.center.x + b.halfSize.x, b.center.y - b.halfSize.y);

            common::math::Vector2 finalLeftUpper = common::math::Vector2(std::min(leftUpper1.x, leftUpper2.x), std::max(leftUpper1.y, leftUpper2.y));
            common::math::Vector2 finalRightLower = common::math::Vector2(std::max(rightLower1.x, rightLower2.x), std::min(rightLower1.y, rightLower2.y));

            common::math::Vector2 aabbHalfSize = common::math::Vector2(std::abs(finalLeftUpper.x - finalRightLower.x) / 2, std::abs(finalLeftUpper.y - finalRightLower.y) / 2);
            common::math::Vector3 aabbCenter = common::math::Vector3(finalLeftUpper.x + aabbHalfSize.x, finalLeftUpper.y - aabbHalfSize.y, zPos);

            this->halfSize = aabbHalfSize;
            this->center = aabbCenter;
        }

    };

    static std::array<common::math::Vector3, 4> calculateVertices3(const AABB2 &aabb, bool moveVertices = false)
    {
        std::array<common::math::Vector3, 4> ret =
        {
            common::math::Vector3(-aabb.halfSize.x, -aabb.halfSize.y, 0.0f),
            common::math::Vector3(-aabb.halfSize.x, aabb.halfSize.y, 0.0f),
            common::math::Vector3(aabb.halfSize.x, aabb.halfSize.y, 0.0f),
            common::math::Vector3(aabb.halfSize.x, -aabb.halfSize.y, 0.0f)
        };

        if (true == moveVertices)
        {
            for (size_t i = 0; i < ret.size(); i++)
            {
                ret[i].x += aabb.center.x;
                ret[i].y += aabb.center.y;
                ret[i].z += aabb.center.z;
            }
        }

        return ret;
    }


    inline float area(const AABB2 &aabb)
    {
        return (aabb.halfSize.x * 2) * (aabb.halfSize.y * 2);
    }
}
}
}
