#pragma once

/* AABB3.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "AABB2.hpp"
#include "Vector3.hpp"

#include <array>

namespace dynamicreflectance {
namespace common{
namespace math
{
    struct AABB3
    {
        common::math::Vector3 center;
        common::math::Vector3 halfSize;

        AABB3()
        {}

        AABB3(common::math::Vector3 center, common::math::Vector3 halfSize, float fatFactor = 0.0f)
            : center(center)
            , halfSize(halfSize)
        {
            this->halfSize.x += halfSize.x * fatFactor;
            this->halfSize.y += halfSize.y * fatFactor;
            this->halfSize.z += halfSize.z * fatFactor;
        }

        AABB3(const AABB2 &aabb2, float depth, float fatFactor = 0.0f)
            : center(aabb2.center)
            , halfSize(aabb2.halfSize.x, aabb2.halfSize.y, depth)
        {
            this->halfSize.x += this->halfSize.x * fatFactor;
            this->halfSize.y += this->halfSize.y * fatFactor;
            this->halfSize.z += this->halfSize.z * fatFactor;
        }

        AABB3(const AABB3 &aabb, float fatFactor = 0.0f)
            : center(aabb.center)
            , halfSize(aabb.halfSize)
        {
            this->halfSize.x += this->halfSize.x * fatFactor;
            this->halfSize.y += this->halfSize.y * fatFactor;
            this->halfSize.z += this->halfSize.z * fatFactor;
        }


        AABB3(const AABB3 &a, const AABB3 &b, float fatFactor = 0.0f)
        {
            common::math::Vector3 frontLeftUpperA = common::math::Vector3(a.center.x - a.halfSize.x, a.center.y + a.halfSize.y, a.center.z - a.halfSize.z);
            common::math::Vector3 rearRightLowerA = common::math::Vector3(a.center.x + a.halfSize.x, a.center.y - a.halfSize.y, a.center.z + a.halfSize.z);
            
            common::math::Vector3 frontLeftUpperB = common::math::Vector3(b.center.x - b.halfSize.x, b.center.y + b.halfSize.y, b.center.z - b.halfSize.z);
            common::math::Vector3 rearRightLowerB = common::math::Vector3(b.center.x + b.halfSize.x, b.center.y - b.halfSize.y, b.center.z + b.halfSize.z);
            
            common::math::Vector3 finalFrontLeftUpper = common::math::Vector3(std::min(frontLeftUpperA.x, frontLeftUpperB.x), std::max(frontLeftUpperA.y, frontLeftUpperB.y), std::min(frontLeftUpperA.z, frontLeftUpperB.z));
            common::math::Vector3 finalRearRightLower = common::math::Vector3(std::max(rearRightLowerA.x, rearRightLowerB.x), std::min(rearRightLowerA.y, rearRightLowerB.y), std::max(rearRightLowerA.z, rearRightLowerB.z));
            
            this->halfSize = common::math::Vector3(std::abs(finalFrontLeftUpper.x - finalRearRightLower.x) * 0.5f, std::abs(finalFrontLeftUpper.y - finalRearRightLower.y) * 0.5f, std::abs(finalFrontLeftUpper.z - finalRearRightLower.z) * 0.5f);
            this->center = common::math::Vector3(finalFrontLeftUpper.x + this->halfSize.x, finalFrontLeftUpper.y - this->halfSize.y, finalFrontLeftUpper.z + this->halfSize.z);

            this->halfSize.x += this->halfSize.x * fatFactor;
            this->halfSize.y += this->halfSize.y * fatFactor;
            this->halfSize.z += this->halfSize.z * fatFactor;
        }
    };

    static std::array<common::math::Vector3, 8> calculateVertices3(const AABB3 &aabb, bool moveVertices = false)
    {
        std::array<common::math::Vector3, 8> ret =
        {
            common::math::Vector3(-aabb.halfSize.x, -aabb.halfSize.y, -aabb.halfSize.z),
            common::math::Vector3(-aabb.halfSize.x, aabb.halfSize.y, -aabb.halfSize.z),
            common::math::Vector3(aabb.halfSize.x, aabb.halfSize.y, -aabb.halfSize.z),
            common::math::Vector3(aabb.halfSize.x, -aabb.halfSize.y, -aabb.halfSize.z),
            common::math::Vector3(-aabb.halfSize.x, -aabb.halfSize.y, aabb.halfSize.z),
            common::math::Vector3(-aabb.halfSize.x, aabb.halfSize.y, aabb.halfSize.z),
            common::math::Vector3(aabb.halfSize.x, aabb.halfSize.y, aabb.halfSize.z),
            common::math::Vector3(aabb.halfSize.x, -aabb.halfSize.y, aabb.halfSize.z)
        };

        if (true == moveVertices)
        {
            for (size_t i = 0; i < ret.size(); i++)
            {
                ret[i].x += aabb.center.x;
                ret[i].y += aabb.center.y;
                ret[i].z += aabb.center.z;
            }
        }

        return ret;
    }

    static bool contains(const AABB3 &a, const AABB3 &b)
    {
        common::math::Vector3 frontLeftUpperA = common::math::Vector3(a.center.x - a.halfSize.x, a.center.y + a.halfSize.y, a.center.z - a.halfSize.z);
        common::math::Vector3 rearRightLowerA = common::math::Vector3(a.center.x + a.halfSize.x, a.center.y - a.halfSize.y, a.center.z + a.halfSize.z);

        common::math::Vector3 frontLeftUpperB = common::math::Vector3(b.center.x - b.halfSize.x, b.center.y + b.halfSize.y, b.center.z - b.halfSize.z);
        common::math::Vector3 rearRightLowerB = common::math::Vector3(b.center.x + b.halfSize.x, b.center.y - b.halfSize.y, b.center.z + b.halfSize.z);

        bool result = true;

        result = result && frontLeftUpperA.x <= frontLeftUpperB.x;
        result = result && frontLeftUpperA.y >= frontLeftUpperB.y;
        result = result && frontLeftUpperA.z <= frontLeftUpperB.z;

        result = result && rearRightLowerA.x >= rearRightLowerB.x;
        result = result && rearRightLowerA.y <= rearRightLowerB.y;
        result = result && rearRightLowerA.z >= rearRightLowerB.z;

        return result;
    }

    inline float area(const AABB3 &aabb)
    {
        return (aabb.halfSize.x * 2) * (aabb.halfSize.y * 2) * (aabb.halfSize.z * 2);
    }
}
}
}