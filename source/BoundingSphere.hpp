#pragma once

/* BoundingSphere.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/


#include "Vector3.hpp"

namespace dynamicreflectance {
namespace common
{
    struct BoudingSphere
    {
        math::Vector3 position;
        float radius;

        BoudingSphere()
            : radius(0.0f)
        {}

        BoudingSphere(const math::Vector3 &position, float radius)
            : position(position)
            , radius(radius)
        {}
    };

}
}