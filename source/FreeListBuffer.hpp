#pragma once

/* FreeListBuffer.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/


#include "Assert.hpp"

#include <cstdint>
#include <vector>
#include <initializer_list>

namespace dynamicreflectance {
namespace common
{

    template<typename T, typename DataContainer, typename FreeListContainer = std::vector<int32_t>>
    class FreeListBuffer
    {
    public:

        FreeListBuffer()
        {}

        FreeListBuffer(const std::initializer_list<T> &list)
            : data(list)
        {}

        ~FreeListBuffer()
        {}

        std::int32_t addBack(const T &data)
        {
            std::int32_t ret;
            if (true == this->freeList.empty())
            {
                this->data.push_back(data);
                ret = (std::int32_t)this->data.size() - 1;
            }
            else
            {
                std::int32_t firstFreeIndex = (std::int32_t)this->freeList[0];
                this->freeList.erase(this->freeList.begin());

                this->data[firstFreeIndex] = data;

                ret = firstFreeIndex;
            }

            return ret;
        }

        std::int32_t getElementsCount() const
        {
            std::int32_t l = (std::int32_t)this->data.size() - (std::int32_t)this->freeList.size();
            return  l >= 0 ? l : 0;
        }

        bool isEmpty() const
        {
            return this->data.empty();
        }

        void remove(std::int32_t index)
        {
            ASSERT(index < (int32_t)this->data.size(), "index out of range");
            ASSERT(index > 0, "index must be positive number");

            this->freeList.push_back(index);
        }

        const T& operator[](std::int32_t index) const
        {
            return this->data[index];
        }

        T& operator[](std::int32_t index)
        {
            return this->data[index];
        }

    private:

        DataContainer data;
        FreeListContainer freeList;
    };
}
}