#pragma once

/* InOut.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

namespace dynamicreflectance {
namespace common
{
    template<typename T>
    class InOut
    {
    public:

        explicit InOut(T &data) : pData(&data) {}
        explicit InOut(T *pData) : pData(pData) {}

        T* getByPointer()
        {
            return this->pData;
        }

        T& getByValue()
        {
            return *(this->pData);
        }

    private:

        T *pData;
    };

    template<typename T>
    InOut<T> inout(T &data) { return InOut<T>(data); }
    template<typename T>
    InOut<T> inout(T *pData) { return InOut<T>(pData); }
    template<typename T>
    InOut<T> inout(InOut<T> data) { return data; }

}
}