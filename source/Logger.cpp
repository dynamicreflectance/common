/* Logger.cpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Logger.hpp"

#include <ctime>
#include <cstdarg>

namespace dynamicreflectance {
namespace common
{

    Logger::Logger(const char *fileName, LoggerSeverity severity, int32_t lineBufferSize, bool writeToStdout)
        : pLineBuffer(new char[(std::size_t)lineBufferSize + 1])
        , lineBufferSize(lineBufferSize + 1)
        , severity(severity)
        , writeToStdout(writeToStdout)
        , fp(nullptr)
    {
        this->fp = fopen(fileName, "w");

        if (nullptr == this->fp)
        {
            this->releaseLineBuffer();
        }
    }

    Logger::~Logger()
    {
        if (nullptr != this->fp)
        {
            fclose(this->fp);
            this->releaseLineBuffer();
        }
    }

    void Logger::info(const EntryLocation &location, const char *format, ...)
    { 
        if (true == this->severity.info)
        {
            va_list args;
            va_start(args, format);
            vsnprintf(this->pLineBuffer, this->lineBufferSize * sizeof(char), format, args);
            va_end(args);

            this->writeAll(this->fp, 'I', location, this->pLineBuffer);

            if (true == this->writeToStdout)
            {
                this->writeAll(stdout, 'I', location, this->pLineBuffer);
            }
        }
    }

    void Logger::warning(const EntryLocation &location, const char *format, ...)
    {
        if (true == this->severity.warning == true)
        {
            va_list args;
            va_start(args, format);
            vsnprintf(this->pLineBuffer, this->lineBufferSize * sizeof(char), format, args);
            va_end(args);

            this->writeAll(this->fp, 'W', location, this->pLineBuffer);

            if (true == this->writeToStdout)
            {
                this->writeAll(stdout, 'W', location, this->pLineBuffer);
            }
        }
    }

    void Logger::error(const EntryLocation &location, const char *format, ...)
    {
        if (true == this->severity.error)
        {
            va_list args;
            va_start(args, format);
            vsnprintf(this->pLineBuffer, this->lineBufferSize * sizeof(char), format, args);
            va_end(args);

            this->writeAll(this->fp, 'E', location, this->pLineBuffer);

            if (true == this->writeToStdout)
            {
                this->writeAll(stdout, 'E', location, this->pLineBuffer);
            }
        }
    }

    void Logger::debug(const EntryLocation &location, const char *format, ...)
    {
        if (true == this->severity.debug)
        {
            va_list args;
            va_start(args, format);
            vsnprintf(this->pLineBuffer, this->lineBufferSize * sizeof(char), format, args);
            va_end(args);

            this->writeAll(this->fp, 'D', location, this->pLineBuffer);

            if (true == this->writeToStdout)
            {
                this->writeAll(stdout, 'D', location, this->pLineBuffer);
            }
        }
    }

    void Logger::assert(const EntryLocation &location, const char *format, ...)
    {
        if (true == this->severity.assert)
        {
            va_list args;
            va_start(args, format);
            vsnprintf(this->pLineBuffer, this->lineBufferSize * sizeof(char), format, args);
            va_end(args);

            this->writeAll(this->fp, 'A', location, this->pLineBuffer);

            if (true == this->writeToStdout)
            {
                this->writeAll(stdout, 'A', location, this->pLineBuffer);
            }
        }

    }

    void Logger::writeAll(FILE *fp, char type, const EntryLocation &location, const char *pMessage)
    {
        this->writeTimestamp(fp);
        this->writeEntryType(fp, type);

        this->writeEntryLocation(fp, location);
        this->writeEntryText(fp, this->pLineBuffer);
        this->writeNewLine(fp);
    }

    void Logger::writeTimestamp(FILE *fp)
    {
        tm *pTimeInfo = nullptr;
        time_t t = time(nullptr);

        pTimeInfo = localtime(&t);

        fprintf(fp,
            "[%.2d.%.2d.%.2d %.2d:%.2d:%.2d] ",
            pTimeInfo->tm_mday,
            pTimeInfo->tm_mon + 1,
            pTimeInfo->tm_year + 1900,
            pTimeInfo->tm_hour,
            pTimeInfo->tm_min,
            pTimeInfo->tm_sec);

        fflush(fp);
    }

    void Logger::writeEntryType(FILE *fp, char type)
    {
        fprintf(fp, "[%c] ", type);
        fflush(fp);
    }

    void Logger::writeEntryLocation(FILE *fp, const EntryLocation &location)
    {
        fprintf(fp, "%s, LOC: %d in %s ", location.fileName.c_str(), location.line, location.functionName.c_str());
        fflush(fp);
    }

    void Logger::writeEntryText(FILE *fp, const char *text)
    {
        fputs(text, fp);
        fflush(fp);
    }

    void Logger::writeNewLine(FILE *fp)
    {
        fputs("\n", fp);
        fflush(fp);
    }

}
}