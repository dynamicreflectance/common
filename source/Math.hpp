#pragma once

/* Math.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <limits>
#include <cstdint>

namespace dynamicreflectance {
namespace common {
namespace math
{
    const float PI = 3.14159265f;
    const float PI_OVER_360 = PI * 360.0f;
    const float PI_INVERSE = 1.0f / PI;
    const float ROUND_ANGLE_DEGREES = 360.0f;
    const float STRAIGHT_ANGLE_DEGREES = 180.0f;
    const float ROUND_ANGLE_DEGREES_INVERSE = 1.0f / ROUND_ANGLE_DEGREES;
    const float STRAIGHT_ANGLE_DEGREES_INVERSE = 1.0f / STRAIGHT_ANGLE_DEGREES;

    template<typename T>
    bool areScalarsEqual(T a, T b, std::int32_t ulp = 1)
    {
        return	std::abs(a - b) <= std::numeric_limits<T>::epsilon() * std::abs(a + b) * ulp ||
                std::abs(a - b) <= std::numeric_limits<T>::min();
    }

    template<typename T>
    T toDegrees(T radians)
    {
        return radians * 180.0f / PI;
    }

    template<typename T>
    T toRadians(T degrees)
    {
        return degrees * PI / 180.0f;
    }

}
}
}