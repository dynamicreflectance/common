#pragma once

/* Out.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

namespace dynamicreflectance {
namespace common
{
    template<typename T>
    class Out
    {
    public:

        explicit Out(T &data) : pData(&data) {}
        explicit Out(T *pData) : pData(pData) {}

        T* getByPointer()
        {
            return this->pData;
        }

        T& getByValue()
        {
            return *(this->pData);
        }

    private:

        T *pData;
    };

    template<typename T>
    Out<T> out(T &data) { return Out<T>(data); }
    template<typename T>
    Out<T> out(T *pData) { return Out<T>(pData); }
    template<typename T>
    Out<T> out(Out<T> data) { return data; }
}
}