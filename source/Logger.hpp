#pragma once

/* Logger.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "EntryLocation.hpp"

#include <cstdio>

namespace dynamicreflectance {
namespace common
{

    struct LoggerSeverity
    {
        bool info;
        bool warning;
        bool error;
        bool debug;
        bool assert;
        
        LoggerSeverity()
            : info(false)
            , warning(false)
            , error(false)
            , debug(false)
            , assert(false)

        {}

        LoggerSeverity(bool info, bool warning, bool error, bool debug, bool assert)
            : info(info)
            , warning(warning)
            , error(error)
            , debug(debug)
            , assert(assert)
        {}
    };

 
    class Logger
    {
    public:

        Logger(const char *fileName, LoggerSeverity severity, int32_t lineBufferSize, bool writeToStdout);
        ~Logger();


        void info(const EntryLocation &location, const char *format, ...);
        void warning(const EntryLocation &location, const char *format, ...);
        void error(const EntryLocation &location, const char *format, ...);
        void debug(const EntryLocation &location, const char *format, ...);
        void assert(const EntryLocation &location, const char *format, ...);


        bool isCreated() const
        {
            return nullptr != this->fp;
        }

    private:

        void releaseLineBuffer()
        {
            delete[] this->pLineBuffer;;
            this->lineBufferSize = 0;
        }

        void writeAll(FILE *fp, char type, const EntryLocation &location, const char *pMessage);

        void writeTimestamp(FILE *fp);
        void writeEntryType(FILE *fp, char type);
        void writeEntryLocation(FILE *fp, const EntryLocation &location);
        void writeEntryText(FILE *fp, const char *text);
        void writeNewLine(FILE *fp);

    private:

        char *pLineBuffer;
        int32_t lineBufferSize;

        LoggerSeverity severity;

        bool writeToStdout;

        FILE *fp;
    };

}
}