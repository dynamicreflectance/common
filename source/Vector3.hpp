#pragma once

/* Vector3.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Math.hpp"
#include "Vector2.hpp"

namespace dynamicreflectance {
namespace common {
namespace math
{
    struct Vector3
    {
        union
        {
            struct
            {
                float x, y, z;
            };

            float linear[3];

            Vector2 xy;
        };

        Vector3()
            : x(0.0f)
            , y(0.0f)
            , z(0.0f)
        {}

        Vector3(float x, float y, float z)
            : x(x)
            , y(y)
            , z(z)
        {}

        Vector3(Vector2 xy, float z)
            : xy(xy)
            , z(z)
        {}

        void set(float x, float y, float z)
        {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        void setZero()
        {
            this->set(0.0f, 0.0f, 0.0f);
        }

        const Vector3& operator += (const Vector3 &b)
        {
            this->x += b.x;
            this->y += b.y;
            this->z += b.z;

            return *this;
        }

        const Vector3& operator -= (const Vector3 &b)
        {
            this->x -= b.x;
            this->y -= b.y;
            this->z -= b.z;

            return *this;
        }
    };

    inline float squaredLength(const Vector3 &v)
    {
        return v.x * v.x + v.y * v.y + v.z * v.z;
    }

    inline float length(const Vector3 &v)
    {
        return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
    }

    inline Vector3 normalize(const Vector3 &v)
    {
        float invLen = 1.0f / length(v);
        return Vector3(v.x * invLen, v.y * invLen, v.z * invLen);
    }

    inline float dot(const Vector3 &a, const Vector3 &b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    inline Vector3 cross(const Vector3 &a, const Vector3 &b)
    {
        return Vector3(a.y * b.z - b.y * a.z, a.z * b.x - b.z * a.x, a.x * b.y - b.x * a.y);
    }

    inline Vector3 abs(const Vector3 &v)
    {
        return Vector3(std::abs(v.x), std::abs(v.y), std::abs(v.z));
    }

    inline Vector3 operator * (const Vector3 &v, float f)
    {
        return Vector3(v.x * f, v.y * f, v.z * f);
    }

    inline Vector3 operator * (float factor, Vector3 rhs)
    {
        return Vector3(rhs.x * factor, rhs.y * factor, rhs.z * factor);
    }

    inline Vector3 operator + (const Vector3 &a, const Vector3 &b)
    {
        return Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    inline Vector3 operator - (const Vector3 &a, const Vector3 &b)
    {
        return Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    inline bool operator == (const Vector3 &a, const Vector3 &b)
    {
        return  areScalarsEqual(a.x, b.x) &&
                areScalarsEqual(a.y, b.y) &&
                areScalarsEqual(a.z, b.z)
            ;
    }

    inline bool operator != (const Vector3 &a, const Vector3 &b)
    {
        return false == (a == b);
    }
}
}
}