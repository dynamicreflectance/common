#pragma once

/* Frustum.cpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Plane.hpp"
#include "AABB3.hpp"

#include <cmath>

namespace dynamicreflectance {
namespace common {
namespace math
{
    struct Frustum
    {
        union
        {
            struct
            {
                Plane near;
                Plane far;
                Plane left;
                Plane right;
                Plane top;
                Plane bottom;
            };
           
            Plane planes[6];
        };
  
        Frustum(){}
        Frustum(Plane near, Plane far, Plane left, Plane right, Plane top, Plane bottom)
            : near(near)
            , far(far)
            , left(left)
            , right(right)
            , top(top)
            , bottom(bottom)
        {}
        Frustum(const Frustum &rhs)
            : near(rhs.near)
            , far(rhs.far)
            , left(rhs.left)
            , right(rhs.right)
            , top(rhs.top)
            , bottom(rhs.bottom)
        {}
        ~Frustum(){}

    };

    inline bool test(const Frustum &frustum, const Vector3 &point)
    {
        bool ret = true;

        for (size_t i = 0; i < 6 && true == ret; i++)
        {
            if (signedDistance(frustum.planes[i], point) < 0)
            {
                ret = false;
            }
        }

        return ret;
    }

    inline bool test(const Frustum &frustum, const AABB3 &aabb)
    {
        bool ret = true;

        for (int32_t i = 0; i < 6 && true == ret; i++)
        {
            float d = aabb.center.x * frustum.planes[i].A +
                      aabb.center.y * frustum.planes[i].B +
                      aabb.center.z * frustum.planes[i].C;

            float r = aabb.halfSize.x * std::abs(frustum.planes[i].A) +
                      aabb.halfSize.y * std::abs(frustum.planes[i].B) +
                      aabb.halfSize.z * std::abs(frustum.planes[i].C);

            float d_p_r = d + r + frustum.planes[i].D;

            if (d_p_r < 0.0f)
            {
                ret = false;
            }
        }

        return ret;
    }


}
}
}