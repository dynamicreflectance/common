#pragma once

/* Vector4.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Math.hpp"
#include "Vector3.hpp"

namespace dynamicreflectance {
namespace common {
namespace math 
{
    struct Vector4
    {
        union
        {
            struct
            {
                float x, y, z, w;
            };

            struct
            {
                Vector2 xy;
                Vector2 zw;
            };

            float linear[4];

            Vector3 xyz;
        };

        Vector4()
            : x(0.0f)
            , y(0.0f)
            , z(0.0f)
            , w(0.0f)
        {}

        Vector4(float x, float y, float z, float w)
            : x(x)
            , y(y)
            , z(z)
            , w(w)
        {}

        Vector4(Vector3 xyz, float w)
            : xyz(xyz)
            , w(w)
        {}

        void set(float x, float y, float z, float w)
        {
            this->x = x;
            this->y = y;
            this->z = z;
            this->w = w;
        }

        void setZero()
        {
            this->set(0.0f, 0.0f, 0.0f, 0.0f);
        }

        const Vector4& operator += (const Vector4 &b)
        {
            this->x += b.x;
            this->y += b.y;
            this->z += b.z;
            this->w += b.w;

            return *this;
        }

        const Vector4& operator -= (const Vector4 &b)
        {
            this->x -= b.x;
            this->y -= b.y;
            this->z -= b.z;
            this->w -= b.w;

            return *this;
        }
    };

    inline float squaredLength(const Vector4 &v)
    {
        return v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w;
    }

    inline float length(const Vector4 &v)
    {
        return sqrt(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w);
    }

    inline Vector4 normalize(const Vector4 &v)
    {
        float invLen = 1.0f / length(v);
        return Vector4(v.x * invLen, v.y * invLen, v.z * invLen, v.w * invLen);
    }

    inline float dot(const Vector4 &a, const Vector4 &b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
    }

    inline Vector4 abs(const Vector4 &v)
    {
        return Vector4(std::abs(v.x), std::abs(v.y), std::abs(v.z), std::abs(v.w));
    }

    inline Vector4 operator * (const Vector4 &v, float f)
    {
        return Vector4(v.x * f, v.y * f, v.z * f, v.w * f);
    }

    inline Vector4 operator * (float factor, Vector4 rhs)
    {
        return Vector4(rhs.x * factor, rhs.y * factor, rhs.z * factor, rhs.w * factor);
    }

    inline Vector4 operator + (const Vector4 &a, const Vector4 &b)
    {
        return Vector4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
    }

    inline Vector4 operator - (const Vector4 &a, const Vector4 &b)
    {
        return Vector4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
    }

    inline bool operator == (const Vector4 &a, const Vector4 &b)
    {
        return  areScalarsEqual(a.x, a.x) &&
                areScalarsEqual(a.y, a.y) &&
                areScalarsEqual(a.z, a.z) &&
                areScalarsEqual(a.w, a.w)
       ;
    }

    inline bool operator != (const Vector4 &a, const Vector4 &b)
    {
        return false == (a == b);
    }
}
}
}