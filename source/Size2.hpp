#pragma once

/* Size2.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <cstdint>

namespace dynamicreflectance {
namespace common
{
    struct Size2
    {
        std::int32_t width;
        std::int32_t height;

        Size2()
            : width(0)
            , height(0)
        {}

        Size2(std::int32_t width, std::int32_t height)
            : width(width)
            , height(height)
        {}

        bool operator == (const Size2 &rhs) const
        {
            return rhs.width == this->width && rhs.width == rhs.width;
        }

        bool operator != (const Size2 &rhs) const
        {
            return false == (*this == rhs);
        }

    };
}
}