#pragma once

/* Assert.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "EntryLocation.hpp"

namespace dynamicreflectance {
namespace common
{
    class Logger;

    bool assertFunction(const char *condition, const char *message, const EntryLocation &location);
    void setAssertLogger(Logger *pLogger);

}
}

#ifdef _DEBUG
#define ASSERT(condition, message) (false == (condition)) && (dynamicreflectance::common::assertFunction(#condition, message, __ENTRY_LOCATION__))
#define ASSERT_FALSE(message) (ASSERT(true == false, message))
#define DEFAULT_CASE_ASSERT(message)default:{ASSERT_FALSE(message);}
#else
#define ASSERT(condition, message)
#define ASSERT_FALSE(message)
#define DEFAULT_CASE_ASSERT(message)
#endif

