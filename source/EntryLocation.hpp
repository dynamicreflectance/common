#pragma once

/* EntryLocation.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <cstdint>
#include <string>

namespace dynamicreflectance {
namespace common
{

    struct EntryLocation
    {
        std::int32_t line;
        std::string functionName;
        std::string fileName;

        EntryLocation()
            : line(0)
        {}

        EntryLocation(std::int32_t _line, const std::string &_functionName, const std::string &_fileName, bool _trimFilePath = true)
            : line(_line)
            , functionName(_functionName)
            , fileName(true == _trimFilePath ? std::string(_fileName.end() - (_fileName.length() - _fileName.find_last_of('\\')) + 1, _fileName.end() ) : _fileName)
        {}
    };

}
}

#define __ENTRY_LOCATION__ dynamicreflectance::common::EntryLocation(__LINE__, __FUNCTION__, __FILE__)