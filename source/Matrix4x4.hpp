#pragma once

/* Matrix4x4.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Vector4.hpp"
#include "InOut.hpp"

namespace dynamicreflectance {
namespace common {
namespace math 
{ 
    struct Matrix4x4
    {
        union
        {
            float linear[4 * 4];
            Vector4 rows[4];
            
            struct
            {
                Vector4 row0;
                Vector4 row1;
                Vector4 row2;
                Vector4 row3;
            };
        };

        Matrix4x4(float diagonalValue = 0.0f)
            : row0(diagonalValue, 0.0f, 0.0f, 0.0f)
            , row1(0.0f, diagonalValue, 0.0f, 0.0f)
            , row2(0.0f, 0.0f, diagonalValue, 0.0f)
            , row3(0.0f, 0.0f, 0.0f, diagonalValue)
        {
        }

        Matrix4x4(Vector4 row0, Vector4 row1, Vector4 row2, Vector4 row3)
            : row0(row0)
            , row1(row1)
            , row2(row2)
            , row3(row3)
        {
        }

        void set(Vector4 row0, Vector4 row1, Vector4 row2, Vector4 row3)
        {
            this->row0 = row0;
            this->row1 = row1;
            this->row2 = row2;
            this->row3 = row3;
        }

        void setZero()
        {
            this->row0.setZero();
            this->row1.setZero();
            this->row2.setZero();
            this->row3.setZero();
        }

        void setIdentity()
        {
            this->row0.set(1.0f, 0.0f, 0.0f, 0.0f);
            this->row1.set(0.0f, 1.0f, 0.0f, 0.0f);
            this->row2.set(0.0f, 0.0f, 1.0f, 0.0f);
            this->row3.set(0.0f, 0.0f, 0.0f, 1.0f);
        }

    };

    inline Matrix4x4 operator + (const Matrix4x4 &a, const Matrix4x4 &b)
    {
        Matrix4x4 ret;

        for (int32_t i = 0; i < 4 * 4; i++)
        {
            ret.linear[i] = a.linear[i] + b.linear[i];
        }

        return ret;
    }

    inline Matrix4x4 operator - (const Matrix4x4 &a, const Matrix4x4 &b)
    {
        Matrix4x4 ret;

        for (int32_t i = 0; i < 4 * 4; i++)
        {
            ret.linear[i] = a.linear[i] - b.linear[i];
        }

        return ret;
    }

    inline Matrix4x4 operator * (const Matrix4x4 &a, float scalar)
    {
        Matrix4x4 ret;

        for (int32_t i = 0; i < 4 * 4; i++)
        {
            ret.linear[i] = a.linear[i] * scalar;
        }

        return ret;
    }

    inline Matrix4x4 operator * (float scalar, const Matrix4x4 &b)
    {
        Matrix4x4 ret;

        for (int32_t i = 0; i < 4 * 4; i++)
        {
            ret.linear[i] = b.linear[i] * scalar;
        }

        return ret;
    }

    inline Matrix4x4 operator * (const Matrix4x4 &a, const Matrix4x4 &b)
    {
        Matrix4x4 ret;

        ret.rows[0] = a.rows[0] * b.rows[0].linear[0] + a.rows[1] * b.rows[0].linear[1] + a.rows[2] * b.rows[0].linear[2] + a.rows[3] * b.rows[0].linear[3];
        ret.rows[1] = a.rows[0] * b.rows[1].linear[0] + a.rows[1] * b.rows[1].linear[1] + a.rows[2] * b.rows[1].linear[2] + a.rows[3] * b.rows[1].linear[3];
        ret.rows[2] = a.rows[0] * b.rows[2].linear[0] + a.rows[1] * b.rows[2].linear[1] + a.rows[2] * b.rows[2].linear[2] + a.rows[3] * b.rows[2].linear[3];
        ret.rows[3] = a.rows[0] * b.rows[3].linear[0] + a.rows[1] * b.rows[3].linear[1] + a.rows[2] * b.rows[3].linear[2] + a.rows[3] * b.rows[3].linear[3];

        return ret;
    }

    inline bool operator == (const Matrix4x4 &a, const Matrix4x4 &b)
    {
        return a.row0 == b.row0 && a.row1 == b.row1 && a.row2 == b.row2 && a.row3 == b.row3;
    }

    inline bool operator != (const Matrix4x4 &a, const Matrix4x4 &b)
    {
        return false == (a == b);
    }

    static bool inverseWithCheck(InOut<Matrix4x4> m)
    {
        Matrix4x4 temp;

        temp.linear[0] = m.getByValue().linear[5] * m.getByValue().linear[10] * m.getByValue().linear[15] -
                         m.getByValue().linear[5] * m.getByValue().linear[11] * m.getByValue().linear[14] -
                         m.getByValue().linear[9] * m.getByValue().linear[6] * m.getByValue().linear[15] +
                         m.getByValue().linear[9] * m.getByValue().linear[7] * m.getByValue().linear[14] +
                         m.getByValue().linear[13] * m.getByValue().linear[6] * m.getByValue().linear[11] -
                         m.getByValue().linear[13] * m.getByValue().linear[7] * m.getByValue().linear[10];

        temp.linear[4] = -m.getByValue().linear[4] * m.getByValue().linear[10] * m.getByValue().linear[15] +
                          m.getByValue().linear[4] * m.getByValue().linear[11] * m.getByValue().linear[14] +
                          m.getByValue().linear[8] * m.getByValue().linear[6] * m.getByValue().linear[15] -
                          m.getByValue().linear[8] * m.getByValue().linear[7] * m.getByValue().linear[14] -
                          m.getByValue().linear[12] * m.getByValue().linear[6] * m.getByValue().linear[11] +
                          m.getByValue().linear[12] * m.getByValue().linear[7] * m.getByValue().linear[10];
                               
        temp.linear[8] =  m.getByValue().linear[4] * m.getByValue().linear[9] * m.getByValue().linear[15] -
                          m.getByValue().linear[4] * m.getByValue().linear[11] * m.getByValue().linear[13] -
                          m.getByValue().linear[8] * m.getByValue().linear[5] * m.getByValue().linear[15] +
                          m.getByValue().linear[8] * m.getByValue().linear[7] * m.getByValue().linear[13] +
                          m.getByValue().linear[12] * m.getByValue().linear[5] * m.getByValue().linear[11] -
                          m.getByValue().linear[12] * m.getByValue().linear[7] * m.getByValue().linear[9];

        temp.linear[12] = -m.getByValue().linear[4] * m.getByValue().linear[9] * m.getByValue().linear[14] +
                           m.getByValue().linear[4] * m.getByValue().linear[10] * m.getByValue().linear[13] +
                           m.getByValue().linear[8] * m.getByValue().linear[5] * m.getByValue().linear[14] -
                           m.getByValue().linear[8] * m.getByValue().linear[6] * m.getByValue().linear[13] -
                           m.getByValue().linear[12] * m.getByValue().linear[5] * m.getByValue().linear[10] +
                           m.getByValue().linear[12] * m.getByValue().linear[6] * m.getByValue().linear[9];

        temp.linear[1] = -m.getByValue().linear[1] * m.getByValue().linear[10] * m.getByValue().linear[15] +
                          m.getByValue().linear[1] * m.getByValue().linear[11] * m.getByValue().linear[14] +
                          m.getByValue().linear[9] * m.getByValue().linear[2] * m.getByValue().linear[15] -
                          m.getByValue().linear[9] * m.getByValue().linear[3] * m.getByValue().linear[14] -
                          m.getByValue().linear[13] * m.getByValue().linear[2] * m.getByValue().linear[11] +
                          m.getByValue().linear[13] * m.getByValue().linear[3] * m.getByValue().linear[10];

        temp.linear[5] = m.getByValue().linear[0] * m.getByValue().linear[10] * m.getByValue().linear[15] -
                         m.getByValue().linear[0] * m.getByValue().linear[11] * m.getByValue().linear[14] -
                         m.getByValue().linear[8] * m.getByValue().linear[2] * m.getByValue().linear[15] +
                         m.getByValue().linear[8] * m.getByValue().linear[3] * m.getByValue().linear[14] +
                         m.getByValue().linear[12] * m.getByValue().linear[2] * m.getByValue().linear[11] -
                         m.getByValue().linear[12] * m.getByValue().linear[3] * m.getByValue().linear[10];

        temp.linear[9] = -m.getByValue().linear[0] * m.getByValue().linear[9] * m.getByValue().linear[15] +
                          m.getByValue().linear[0] * m.getByValue().linear[11] * m.getByValue().linear[13] +
                          m.getByValue().linear[8] * m.getByValue().linear[1] * m.getByValue().linear[15] -
                          m.getByValue().linear[8] * m.getByValue().linear[3] * m.getByValue().linear[13] -
                          m.getByValue().linear[12] * m.getByValue().linear[1] * m.getByValue().linear[11] +
                          m.getByValue().linear[12] * m.getByValue().linear[3] * m.getByValue().linear[9];

        temp.linear[13] = m.getByValue().linear[0] * m.getByValue().linear[9] * m.getByValue().linear[14] -
                          m.getByValue().linear[0] * m.getByValue().linear[10] * m.getByValue().linear[13] -
                          m.getByValue().linear[8] * m.getByValue().linear[1] * m.getByValue().linear[14] +
                          m.getByValue().linear[8] * m.getByValue().linear[2] * m.getByValue().linear[13] +
                          m.getByValue().linear[12] * m.getByValue().linear[1] * m.getByValue().linear[10] -
                          m.getByValue().linear[12] * m.getByValue().linear[2] * m.getByValue().linear[9];

        temp.linear[2] = m.getByValue().linear[1] * m.getByValue().linear[6] * m.getByValue().linear[15] -
                         m.getByValue().linear[1] * m.getByValue().linear[7] * m.getByValue().linear[14] -
                         m.getByValue().linear[5] * m.getByValue().linear[2] * m.getByValue().linear[15] +
                         m.getByValue().linear[5] * m.getByValue().linear[3] * m.getByValue().linear[14] +
                         m.getByValue().linear[13] * m.getByValue().linear[2] * m.getByValue().linear[7] -
                         m.getByValue().linear[13] * m.getByValue().linear[3] * m.getByValue().linear[6];

        temp.linear[6] = -m.getByValue().linear[0] * m.getByValue().linear[6] * m.getByValue().linear[15] +
                          m.getByValue().linear[0] * m.getByValue().linear[7] * m.getByValue().linear[14] +
                          m.getByValue().linear[4] * m.getByValue().linear[2] * m.getByValue().linear[15] -
                          m.getByValue().linear[4] * m.getByValue().linear[3] * m.getByValue().linear[14] -
                          m.getByValue().linear[12] * m.getByValue().linear[2] * m.getByValue().linear[7] +
                          m.getByValue().linear[12] * m.getByValue().linear[3] * m.getByValue().linear[6];

        temp.linear[10] = m.getByValue().linear[0] * m.getByValue().linear[5] * m.getByValue().linear[15] -
                          m.getByValue().linear[0] * m.getByValue().linear[7] * m.getByValue().linear[13] -
                          m.getByValue().linear[4] * m.getByValue().linear[1] * m.getByValue().linear[15] +
                          m.getByValue().linear[4] * m.getByValue().linear[3] * m.getByValue().linear[13] +
                          m.getByValue().linear[12] * m.getByValue().linear[1] * m.getByValue().linear[7] -
                          m.getByValue().linear[12] * m.getByValue().linear[3] * m.getByValue().linear[5];

        temp.linear[14] = -m.getByValue().linear[0] * m.getByValue().linear[5] * m.getByValue().linear[14] +
                           m.getByValue().linear[0] * m.getByValue().linear[6] * m.getByValue().linear[13] +
                           m.getByValue().linear[4] * m.getByValue().linear[1] * m.getByValue().linear[14] -
                           m.getByValue().linear[4] * m.getByValue().linear[2] * m.getByValue().linear[13] -
                           m.getByValue().linear[12] * m.getByValue().linear[1] * m.getByValue().linear[6] +
                           m.getByValue().linear[12] * m.getByValue().linear[2] * m.getByValue().linear[5];

        temp.linear[3] = -m.getByValue().linear[1] * m.getByValue().linear[6] * m.getByValue().linear[11] +
                          m.getByValue().linear[1] * m.getByValue().linear[7] * m.getByValue().linear[10] +
                          m.getByValue().linear[5] * m.getByValue().linear[2] * m.getByValue().linear[11] -
                          m.getByValue().linear[5] * m.getByValue().linear[3] * m.getByValue().linear[10] -
                          m.getByValue().linear[9] * m.getByValue().linear[2] * m.getByValue().linear[7] +
                          m.getByValue().linear[9] * m.getByValue().linear[3] * m.getByValue().linear[6];

        temp.linear[7] = m.getByValue().linear[0] * m.getByValue().linear[6] * m.getByValue().linear[11] -
                         m.getByValue().linear[0] * m.getByValue().linear[7] * m.getByValue().linear[10] -
                         m.getByValue().linear[4] * m.getByValue().linear[2] * m.getByValue().linear[11] +
                         m.getByValue().linear[4] * m.getByValue().linear[3] * m.getByValue().linear[10] +
                         m.getByValue().linear[8] * m.getByValue().linear[2] * m.getByValue().linear[7] -
                         m.getByValue().linear[8] * m.getByValue().linear[3] * m.getByValue().linear[6];

        temp.linear[11] = -m.getByValue().linear[0] * m.getByValue().linear[5] * m.getByValue().linear[11] +
                           m.getByValue().linear[0] * m.getByValue().linear[7] * m.getByValue().linear[9] +
                           m.getByValue().linear[4] * m.getByValue().linear[1] * m.getByValue().linear[11] -
                           m.getByValue().linear[4] * m.getByValue().linear[3] * m.getByValue().linear[9] -
                           m.getByValue().linear[8] * m.getByValue().linear[1] * m.getByValue().linear[7] +
                           m.getByValue().linear[8] * m.getByValue().linear[3] * m.getByValue().linear[5];

        temp.linear[15] = m.getByValue().linear[0] * m.getByValue().linear[5] * m.getByValue().linear[10] -
                          m.getByValue().linear[0] * m.getByValue().linear[6] * m.getByValue().linear[9] -
                          m.getByValue().linear[4] * m.getByValue().linear[1] * m.getByValue().linear[10] +
                          m.getByValue().linear[4] * m.getByValue().linear[2] * m.getByValue().linear[9] +
                          m.getByValue().linear[8] * m.getByValue().linear[1] * m.getByValue().linear[6] -
                          m.getByValue().linear[8] * m.getByValue().linear[2] * m.getByValue().linear[5];

        float det = m.getByValue().linear[0] * temp.linear[0] + m.getByValue().linear[1] * temp.linear[4] + m.getByValue().linear[2] * temp.linear[8] + m.getByValue().linear[3] * temp.linear[12];
        bool ret = false == areScalarsEqual(det, 0.0f);

        if (true == ret)
        {
            det = 1.0f / det;

            for (int32_t i = 0; i < 4 * 4; i++)
            {
                temp.linear[i] = temp.linear[i] * det;
            }

            m.getByValue() = temp;
        }

        return ret;
    }

    static Matrix4x4 inverse(const Matrix4x4 &m)
    {
        Matrix4x4 temp;

        temp.linear[0] = m.linear[5] * m.linear[10] * m.linear[15] -
                         m.linear[5] * m.linear[11] * m.linear[14] -
                         m.linear[9] * m.linear[6] * m.linear[15] +
                         m.linear[9] * m.linear[7] * m.linear[14] +
                         m.linear[13] * m.linear[6] * m.linear[11] -
                         m.linear[13] * m.linear[7] * m.linear[10];

        temp.linear[4] = -m.linear[4] * m.linear[10] * m.linear[15] +
                          m.linear[4] * m.linear[11] * m.linear[14] +
                          m.linear[8] * m.linear[6] * m.linear[15] -
                          m.linear[8] * m.linear[7] * m.linear[14] -
                          m.linear[12] * m.linear[6] * m.linear[11] +
                          m.linear[12] * m.linear[7] * m.linear[10];
                               
        temp.linear[8] =  m.linear[4] * m.linear[9] * m.linear[15] -
                          m.linear[4] * m.linear[11] * m.linear[13] -
                          m.linear[8] * m.linear[5] * m.linear[15] +
                          m.linear[8] * m.linear[7] * m.linear[13] +
                          m.linear[12] * m.linear[5] * m.linear[11] -
                          m.linear[12] * m.linear[7] * m.linear[9];

        temp.linear[12] = -m.linear[4] * m.linear[9] * m.linear[14] +
                           m.linear[4] * m.linear[10] * m.linear[13] +
                           m.linear[8] * m.linear[5] * m.linear[14] -
                           m.linear[8] * m.linear[6] * m.linear[13] -
                           m.linear[12] * m.linear[5] * m.linear[10] +
                           m.linear[12] * m.linear[6] * m.linear[9];

        temp.linear[1] = -m.linear[1] * m.linear[10] * m.linear[15] +
                          m.linear[1] * m.linear[11] * m.linear[14] +
                          m.linear[9] * m.linear[2] * m.linear[15] -
                          m.linear[9] * m.linear[3] * m.linear[14] -
                          m.linear[13] * m.linear[2] * m.linear[11] +
                          m.linear[13] * m.linear[3] * m.linear[10];

        temp.linear[5] = m.linear[0] * m.linear[10] * m.linear[15] -
                         m.linear[0] * m.linear[11] * m.linear[14] -
                         m.linear[8] * m.linear[2] * m.linear[15] +
                         m.linear[8] * m.linear[3] * m.linear[14] +
                         m.linear[12] * m.linear[2] * m.linear[11] -
                         m.linear[12] * m.linear[3] * m.linear[10];

        temp.linear[9] = -m.linear[0] * m.linear[9] * m.linear[15] +
                          m.linear[0] * m.linear[11] * m.linear[13] +
                          m.linear[8] * m.linear[1] * m.linear[15] -
                          m.linear[8] * m.linear[3] * m.linear[13] -
                          m.linear[12] * m.linear[1] * m.linear[11] +
                          m.linear[12] * m.linear[3] * m.linear[9];

        temp.linear[13] = m.linear[0] * m.linear[9] * m.linear[14] -
                          m.linear[0] * m.linear[10] * m.linear[13] -
                          m.linear[8] * m.linear[1] * m.linear[14] +
                          m.linear[8] * m.linear[2] * m.linear[13] +
                          m.linear[12] * m.linear[1] * m.linear[10] -
                          m.linear[12] * m.linear[2] * m.linear[9];

        temp.linear[2] = m.linear[1] * m.linear[6] * m.linear[15] -
                         m.linear[1] * m.linear[7] * m.linear[14] -
                         m.linear[5] * m.linear[2] * m.linear[15] +
                         m.linear[5] * m.linear[3] * m.linear[14] +
                         m.linear[13] * m.linear[2] * m.linear[7] -
                         m.linear[13] * m.linear[3] * m.linear[6];

        temp.linear[6] = -m.linear[0] * m.linear[6] * m.linear[15] +
                          m.linear[0] * m.linear[7] * m.linear[14] +
                          m.linear[4] * m.linear[2] * m.linear[15] -
                          m.linear[4] * m.linear[3] * m.linear[14] -
                          m.linear[12] * m.linear[2] * m.linear[7] +
                          m.linear[12] * m.linear[3] * m.linear[6];

        temp.linear[10] = m.linear[0] * m.linear[5] * m.linear[15] -
                          m.linear[0] * m.linear[7] * m.linear[13] -
                          m.linear[4] * m.linear[1] * m.linear[15] +
                          m.linear[4] * m.linear[3] * m.linear[13] +
                          m.linear[12] * m.linear[1] * m.linear[7] -
                          m.linear[12] * m.linear[3] * m.linear[5];

        temp.linear[14] = -m.linear[0] * m.linear[5] * m.linear[14] +
                           m.linear[0] * m.linear[6] * m.linear[13] +
                           m.linear[4] * m.linear[1] * m.linear[14] -
                           m.linear[4] * m.linear[2] * m.linear[13] -
                           m.linear[12] * m.linear[1] * m.linear[6] +
                           m.linear[12] * m.linear[2] * m.linear[5];

        temp.linear[3] = -m.linear[1] * m.linear[6] * m.linear[11] +
                          m.linear[1] * m.linear[7] * m.linear[10] +
                          m.linear[5] * m.linear[2] * m.linear[11] -
                          m.linear[5] * m.linear[3] * m.linear[10] -
                          m.linear[9] * m.linear[2] * m.linear[7] +
                          m.linear[9] * m.linear[3] * m.linear[6];

        temp.linear[7] = m.linear[0] * m.linear[6] * m.linear[11] -
                         m.linear[0] * m.linear[7] * m.linear[10] -
                         m.linear[4] * m.linear[2] * m.linear[11] +
                         m.linear[4] * m.linear[3] * m.linear[10] +
                         m.linear[8] * m.linear[2] * m.linear[7] -
                         m.linear[8] * m.linear[3] * m.linear[6];

        temp.linear[11] = -m.linear[0] * m.linear[5] * m.linear[11] +
                           m.linear[0] * m.linear[7] * m.linear[9] +
                           m.linear[4] * m.linear[1] * m.linear[11] -
                           m.linear[4] * m.linear[3] * m.linear[9] -
                           m.linear[8] * m.linear[1] * m.linear[7] +
                           m.linear[8] * m.linear[3] * m.linear[5];

        temp.linear[15] = m.linear[0] * m.linear[5] * m.linear[10] -
                          m.linear[0] * m.linear[6] * m.linear[9] -
                          m.linear[4] * m.linear[1] * m.linear[10] +
                          m.linear[4] * m.linear[2] * m.linear[9] +
                          m.linear[8] * m.linear[1] * m.linear[6] -
                          m.linear[8] * m.linear[2] * m.linear[5];

        float det = m.linear[0] * temp.linear[0] + m.linear[1] * temp.linear[4] + m.linear[2] * temp.linear[8] + m.linear[3] * temp.linear[12];

        det = 1.0f / det;

        for (int32_t i = 0; i < 4 * 4; i++)
        {
            temp.linear[i] = temp.linear[i] * det;
        }

        return temp;
    }


    static Matrix4x4 transpose(const Matrix4x4 &m)
    {
        Matrix4x4 ret;

        ret.row0.linear[0] = m.row0.linear[0];
        ret.row0.linear[1] = m.row1.linear[0];
        ret.row0.linear[2] = m.row2.linear[0];
        ret.row0.linear[3] = m.row3.linear[0];

        ret.row1.linear[0] = m.row0.linear[1];
        ret.row1.linear[1] = m.row1.linear[1];
        ret.row1.linear[2] = m.row2.linear[1];
        ret.row1.linear[3] = m.row3.linear[1];

        ret.row2.linear[0] = m.row0.linear[2];
        ret.row2.linear[1] = m.row1.linear[2];
        ret.row2.linear[2] = m.row2.linear[2];
        ret.row2.linear[3] = m.row3.linear[2];

        ret.row3.linear[0] = m.row0.linear[3];
        ret.row3.linear[1] = m.row1.linear[3];
        ret.row3.linear[2] = m.row2.linear[3];
        ret.row3.linear[3] = m.row3.linear[3];

        return ret;
    }

    static void transpose(InOut<Matrix4x4> m)
    {
        m.getByValue() = transpose(m.getByValue());
    }

}
}
}


