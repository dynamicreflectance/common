#pragma once

/* Version.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <string>

namespace dynamicreflectance {
namespace common
{
    struct Version
    {
        int32_t number;
        int32_t year;

        Version()
            : number(0)
            , year(0)
        {}

        Version(int32_t number, int32_t year)
            : number(number)
            , year(year)
        {}

        std::string toString()
        {
            return std::to_string(number) + "." + std::to_string(year);
        }
    };
}
}