#pragma once

/* ImageData.hpp
*
* Copyright (C) 2016 - 2017 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "ColorFormat.hpp"
#include "Size2.hpp"

#include <vector>
#include <cstdint>

namespace dynamicreflectance {
namespace common
{

    struct ImageData
    {
        Size2 resolution;
        ColorFormat format;

        std::int32_t dataLength;
        std::uint8_t *pData;

        ImageData()
            : format(ColorFormat::_UNKOWN)
            , pData(nullptr)
            , dataLength(0)
        {}

        ImageData(const Size2 &resolution, ColorFormat format, const std::uint8_t *pData)
            : resolution(resolution)
            , format(format)
            , pData(new std::uint8_t[resolution.width * resolution.height * getChannelsCount(format) * sizeof(std::uint8_t)])
            , dataLength(resolution.width * resolution.height * getChannelsCount(format) * sizeof(std::uint8_t))
        {
            if (nullptr != pData)
            {
                memcpy(this->pData, pData, this->dataLength);
            }
        }

        ImageData(std::int32_t width, std::int32_t height, ColorFormat format, const std::uint8_t *pData)
            : resolution(width, height)
            , format(format)
            , pData(new std::uint8_t[width * height * getChannelsCount(format) * sizeof(std::uint8_t)])
            , dataLength(width * height * getChannelsCount(format) * sizeof(std::uint8_t))
        {
            if (nullptr != pData)
            {
                memcpy(this->pData, pData, this->dataLength);
            }
        }

        ImageData(const ImageData &rhs)
            : resolution(rhs.resolution)
            , format(rhs.format)
            , pData(new std::uint8_t[(std::size_t)rhs.dataLength])
            , dataLength(rhs.dataLength)
        {
            ASSERT(0 < rhs.dataLength, "Incorrect data length");
            memcpy(this->pData, rhs.pData, (std::size_t)this->dataLength);
        }

        const ImageData& operator = (const ImageData &rhs)
        {
            if (this != &rhs)
            {
                delete[] this->pData; this->pData = nullptr;

                this->pData = new std::uint8_t[(std::size_t)rhs.dataLength];
                this->dataLength = rhs.dataLength;
                this->resolution = rhs.resolution;

                memcpy(this->pData, rhs.pData, this->dataLength * sizeof(std::uint8_t));
            }

            return *this;
        }

        ~ImageData()
        {
            delete[] this->pData;

            this->dataLength = 0;
            this->format = ColorFormat::_UNKOWN;
            this->resolution.width = 0;
            this->resolution.height = 0;
        }
    };

}
}